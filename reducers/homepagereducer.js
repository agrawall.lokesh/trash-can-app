import {
    SET_SHOW_CAMERA_COMPONENT,
    SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
    CLEAR_SHARE_IMAGE_STATE,
    NAVIGATE_TO_HOME_SCREEN,
    UPDATE_IMAGE_COUNT,
    INCREMENT_IMAGE_COUNT,
    SET_IMAGE_TAGS,
    SET_TAG_TEXT,
    SET_ACTIVE_GRAPH_TAB,
    UPDATE_GRAPH_IMAGE_COUNT,
    S3_UPLOAD_ERROR,
    MONGO_UPLOAD_ERROR,
    REFRESH_GRAPH_DATA_ERROR
} from '../actions/types';

export default function (state = {
    showCameraComponent: false,
    capturedImage: null,
    capturedImageBase64: null,
    navigateToHomeScreen: false,
    trashImageCount: 0,
    trashGraphImageCountData: null,
    imageTags: null,
    clarifaiTags: null,
    userTags: null,
    tagText: ' ',
    activeTabIndex: 0
}, action) {
    switch (action.type) {
        case SET_SHOW_CAMERA_COMPONENT:
            return {...state, showCameraComponent: true};
        case SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA:
            return {
                ...state,
                capturedImage: action.image.uri,
                capturedImageBase64: action.image.base64,
                showCameraComponent: false
            };
        case CLEAR_SHARE_IMAGE_STATE:
            return {
                ...state,
                showCameraComponent: false,
                capturedImage: null,
                imageBase64: null,
                navigateToHomeScreen: true,
                imageTags: null,
                clarifaiTags: null,
                userTags: null,
                tagText: ' '
            };
        case NAVIGATE_TO_HOME_SCREEN:
            return {
                ...state,
                showCameraComponent: false,
                capturedImage: null,
                imageBase64: null,
                navigateToHomeScreen: false
            };
        case UPDATE_IMAGE_COUNT:
            return {
                ...state,
                trashImageCount: action.trashImageCount
            };
        case UPDATE_GRAPH_IMAGE_COUNT:
            return {
                ...state,
                trashGraphImageCountData: action.trashImageData.data
            };
        case INCREMENT_IMAGE_COUNT:
            return {
                ...state,
                trashImageCount: state.trashImageCount + 1
            };
        case SET_IMAGE_TAGS:
            return {
                ...state,
                clarifaiTags: action.clarifaiTags,
                userTags: action.userTags,
                imageTags: action.clarifaiTags.concat(action.userTags),
                tagText: action.tagText
            };
        case SET_TAG_TEXT:
            return {
                ...state,
                tagText: action.tagText
            };
        case SET_ACTIVE_GRAPH_TAB:
            return {
                ...state,
                activeTabIndex: action.activeTabIndex
            };
        case S3_UPLOAD_ERROR:
        case MONGO_UPLOAD_ERROR:
        case REFRESH_GRAPH_DATA_ERROR:
            console.log(action.error);
            return state;
        default:
            return state;
    }
}