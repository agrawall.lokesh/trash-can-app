import {USER_LOCATION} from '../actions/types';

const INITIAL_STATE = {
    location: {
        "coords": {
            latitude: 18,
            longitude: 78,
        }
    }
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case USER_LOCATION:
            return {location: action.location};
        default:
            return state
    }
}
