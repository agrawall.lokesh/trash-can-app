import  {MAP_POINTS, UPDATE_MARKERS , UPDATE_IMAGE_URLS, MODAL_VISIBLE, PIC_MODAL_VISIBLE, PIC_MODAL} from '../actions/types';

const INITIAL_STATE = {
    markers: [],
    isModalVisible:false,
    imageUrlArray:[],
    isPicModalVisible:false
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case MAP_POINTS:
            return {...state, ...action.payload};
        case UPDATE_MARKERS:
        	return {...state,...action.payload};
        case UPDATE_IMAGE_URLS:
        	
        	return {...state,imageUrlArray:action.payload.imageUrlArray};
        case MODAL_VISIBLE:
        	return {...state,isModalVisible:action.payload.isModalVisible};
        case PIC_MODAL_VISIBLE:
            return {...state,isPicModalVisible:action.payload.isPicModalVisible};
        case PIC_MODAL:
            return {...state,...action.payload}
        default:
            return state;
    }
}
