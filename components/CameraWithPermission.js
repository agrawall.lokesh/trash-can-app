import {Permissions} from "expo";

import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from "react-redux";

import Camera from './Camera';
import {navigateToHomeScreen} from "../actions/homepageactions";

class CameraWithPermission extends Component {
    state = {
        hasCameraPermission: false,
    };

    async componentWillMount() {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.setState({hasCameraPermission: true});
        }
        else {
            this.props.navigateToHomeScreen();
        }
    }

    render() {
        if (this.state.hasCameraPermission) {
            return (
                <View>
                    <Camera onImageCapture={this.props.onImageCapture}/>
                </View>
            )
        }
        return <View/>
    }
}

export default connect(null, {
    navigateToHomeScreen
})(CameraWithPermission);
