import React from 'react';

import {TabNavigator, StackNavigator} from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import Map from '../screens/MapScreen';
import ShareImageScreen from '../screens/ShareImageScreen';

import GLOBAL from '../Globals'

import {
    Icon
} from 'react-native-elements';

const Drawer = TabNavigator({
    Home: StackNavigator({
        home_screen: {screen: HomeScreen},
        share_image_screen: {
            screen: ShareImageScreen
        }
    }),
    Map: {screen: Map}
}, {
    navigationOptions: ({navigation}) => ({
        tabBarIcon: ({focused, tintColor}) => {
            const {routeName} = navigation.state;
            let iconName;
            if (routeName === 'Home') {
                iconName = `home`;
            } else if (routeName === 'Map') {
                iconName = `map`;
            }

            return <Icon name={iconName} size={30} color={tintColor}/>;
        },
    }),
    headerMode: "screen",
    headerTintColor: GLOBAL.COLOR.PRIMARY,
    headerTitleStyle: {
        color: GLOBAL.COLOR.PRIMARY
    },
    tabBarPosition: 'bottom',
    tabBarOptions: {
        activeTintColor: "#5846C9",
        inactiveTintColor: "#000000",
        showLabel: false,
        showIcon: true,
        style: {
            backgroundColor: "#ffffff",
        },
    },
    animationEnabled: true,
    swipeEnabled: false,
});

export default Drawer;