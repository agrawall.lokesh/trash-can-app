import React, {Component} from 'react';
import {connect} from "react-redux";
import Geocoder from 'react-native-geocoding';
import {Icon} from 'react-native-elements';
import Clarifai from 'clarifai';
import ImageTags from './ImageTags'

import {Header} from 'react-navigation';

import GLOBALS from '../Globals';

import {
    StyleSheet,
    View,
    Image,
    Share,
    Dimensions,
} from 'react-native';

import HeaderImageScrollView from 'react-native-image-header-scroll-view';

import {
    uploadImageToServer,
    setImageTags
} from "../actions/homepageactions";

const MIN_HEIGHT = Header.HEIGHT;
const MAX_HEIGHT = Dimensions.get('window').height * 0.5;

class PreviewImage extends Component {
    componentDidMount() {
        const clarifai = new Clarifai.App({
            apiKey: GLOBALS.CLARIFAI.API_KEY
        });

        if (this.props.capturedImageBase64) {
            clarifai.models.predict(GLOBALS.CLARIFAI.MODEL, {base64: this.props.capturedImageBase64}).then(
                ({outputs}) => {
                    let clarifaiConceptsList = outputs[0].data.concepts;
                    let significantConcepts = clarifaiConceptsList.filter(
                        (concept) => concept.value > GLOBALS.CLARIFAI.IMAGE_ACCURACY_THRESHOLD
                    );

                    let significantConceptNames = significantConcepts.map((concept) => concept.name);

                    if (significantConceptNames.length === 0) {
                        this.props.setImageTags([], [], ' ')
                    }
                    else {
                        this.props.setImageTags(significantConceptNames, [], ' ')
                    }
                },
                (err) => {
                    console.log(err);
                }
            );
        }
        else {
            console.log("Captured Image is nil");
        }
    }

    getLocationFromObjects = (location) => {
        const locality = location.map((loc) => {
            return loc.long_name
        });

        const address = locality.join('');
        const addressWithoutSpaces = address.replace(/\s+/g, "");
        return "#TrashCan " + "#" + addressWithoutSpaces;
    };

    onShareClick = async () => {
        let lat = this.props.location.coords.latitude;
        let lon = this.props.location.coords.longitude;

        Geocoder.setApiKey(GLOBALS.GEO_CODER.API_KEY);
        Geocoder.getFromLatLng(lat, lon).then(
            ({results}) => {
                let address_component = results[0].address_components;
                let location_string = this.getLocationFromObjects(address_component);

                let imageTags = '#' + this.props.imageTags.join(' #');

                location_string += " " + imageTags;

                Share.share({
                        message: location_string,
                        title: 'share',
                        url: this.props.capturedImage
                    }, {
                        dialogTitle: 'This is share dialog title',
                        tintColor: 'green'
                    }
                ).then(() => {
                    this.props.uploadImageToServer(this.props.capturedImageBase64, this.props.location);
                });
            },
            error => {
                alert(error);
            }
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <HeaderImageScrollView
                    maxHeight={MAX_HEIGHT}
                    minHeight={MIN_HEIGHT}
                    maxOverlayOpacity={0.6}
                    minOverlayOpacity={0.3}
                    fadeOutForeground
                    scrollViewBackgroundColor={GLOBALS.COLOR.PRIMARY}
                    showsVerticalScrollIndicator={false}
                    renderHeader={() =>
                        <Image
                            source={{uri: this.props.capturedImage}}
                            resizeMode="cover"
                            style={styles.image}/>}
                >
                    <View style={[styles.container, styles.imageTagsView]}>
                        <ImageTags initialText=""
                                   initialTags={this.props.clarifaiTags}
                                   inputStyle={{backgroundColor: 'white'}}/>
                    </View>
                </HeaderImageScrollView>
                <View style={styles.shareImageButtonView}>
                    <Icon
                        reverse
                        raised
                        name="share"
                        type='feather'
                        iconStyle={{color: GLOBALS.COLOR.TERTIARY}}
                        color={GLOBALS.COLOR.SECONDARY}
                        onPress={() => this.onShareClick()}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps({homePageReducer, locationReducer}) {
    return {
        capturedImage: homePageReducer.capturedImage,
        capturedImageBase64: homePageReducer.capturedImageBase64,
        location: locationReducer.location,
        imageTags: homePageReducer.imageTags,
        clarifaiTags: homePageReducer.clarifaiTags
    }
}

export default connect(mapStateToProps,
    {uploadImageToServer, setImageTags})(PreviewImage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GLOBALS.COLOR.PRIMARY,
    },
    navTitleView: {
        height: MIN_HEIGHT,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 24,
        opacity: 0,
    },
    image: {
        height: MAX_HEIGHT,
        width: Dimensions.get('window').width,
        alignSelf: 'stretch',
    },
    imageTagsView: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: GLOBALS.COLOR.PRIMARY,
    },
    shareImageButtonView: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 20
    }
});