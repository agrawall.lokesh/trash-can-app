import React from 'react';
import PropTypes from 'prop-types';
import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native';

import GLOBAL from '../Globals'

import {setImageTags, setTagText} from "../actions/homepageactions";

import {connect} from "react-redux";

const Tag = ({label, onPress}) => {
    return (
        <TouchableOpacity style={[styles.tag]} onPress={onPress}>
            <Text style={[styles.tagLabel]}>
                #{label}
            </Text>
        </TouchableOpacity>
    );
};

Tag.propTypes = {
    label: PropTypes.string.isRequired,
    onPress: PropTypes.func
};

class ImageTags extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeText = this.onChangeText.bind(this);
    }

    deleteTextTags() {
        this.props.setImageTags(
            this.props.initialTags,
            this.props.userTags.slice(0, -1),
            this.props.userTags.slice(-1)[0] || ' ');
    }

    createTextTag(text) {
        this.props.setImageTags(
            this.props.initialTags,
            [...this.props.userTags, text.slice(0, -1).trim()],
            ' ');
    }

    onChangeText(text) {
        if (text.length === 0) {
            this.deleteTextTags();
        }
        else if ((text.length > 1) && (text.slice(-1) === ' ') && text.trim()) {
            this.createTextTag(text)
        } else {
            this.props.setTagText(text);
        }
    }

    render() {
        if (this.props.initialTags === null || this.props.initialTags.length === 0) {
            return (
                <View>
                    <Text style={styles.tagLoading}>Loading Tags...</Text>
                </View>
            )
        }

        return (
            <View style={[styles.container]}>
                {this.props.initialTags.map((tag, i) => (
                    <Tag
                        key={i}
                        label={tag}/>)
                )}
                {this.props.userTags.map((tag, i) => (
                    <Tag
                        key={i}
                        label={tag}/>)
                )}
                <View style={[styles.textInputContainer]}>
                    <TextInput
                        value={this.props.tagText}
                        style={[styles.textInput, this.props.inputStyle]}
                        onChangeText={this.onChangeText}
                        underlineColorAndroid="transparent"/>
                </View>
            </View>
        );
    }
}

function mapStateToProps({homePageReducer}) {
    return {
        userTags: homePageReducer.userTags,
        clarifaiTags: homePageReducer.clarifaiTags,
        tagText: homePageReducer.tagText
    }
}

export default connect(mapStateToProps, {setImageTags, setTagText})(ImageTags);

ImageTags.defaultProps = {
    inputStyle: {}
};

ImageTags.propTypes = {
    initialTags: PropTypes.arrayOf(PropTypes.string),
    inputStyle: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },

    textInputContainer: {
        flex: 1,
        width: 100,
        height: 32,
        margin: 4,
        borderRadius: 16,
        backgroundColor: GLOBAL.COLOR.TERTIARY,
    },

    textInput: {
        margin: 0,
        padding: 0,
        paddingLeft: 12,
        paddingRight: 12,
        flex: 1,
        height: 32,
        fontSize: 13,
        color: 'rgba(0, 0, 0, 0.87)'
    },

    tag: {
        justifyContent: 'center',
        backgroundColor: GLOBAL.COLOR.TERTIARY,
        borderRadius: 16,
        paddingLeft: 12,
        paddingRight: 12,
        height: 32,
        margin: 4,
    },

    tagLabel: {
        fontSize: 13,
        color: 'rgba(0, 0, 0, 0.87)',
    },

    tagLoading: {
        margin: 10,
        padding: 10,
        fontSize: 14,
        textAlign: 'center',
        fontWeight: 'bold',
        color: GLOBAL.COLOR.SECONDARY
    },
});