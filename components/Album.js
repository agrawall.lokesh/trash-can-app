import React, {Component} from 'react';
import {
    StyleSheet,
    FlatList,
    Dimensions,
    TouchableOpacity
} from 'react-native';

import {updateModalVisibility, showPicModal,updatePicModal} from '../actions/mapactions'
import GLOBAL from '../Globals';
import {
    connect
} from 'react-redux';
import {Image} from 'react-native-expo-image-cache';


class Album extends Component {
    constructor(props) {
        super(props);
    }

    renderImage = ({item, index}) =>{
        return (
             <TouchableOpacity onPress={() => {
                                    this.props.showPicModal(item);
                                    this.props.updatePicModal(true);
                                }}
                style={{flex:1}} >
            <Image key = {index.toString()} style={styles.imagePallet} {...{uri:item}} />
            </TouchableOpacity>
            );
    }

    render() {

        return (
            
            <FlatList
            contentContainerStyle={styles.list}
            data={this.props.data.slice(0,30)}
            renderItem={this.renderImage}
            numColumns={3}
            keyExtractor={(item, index) => index}
            style = {{backgroundColor:"white"}}/>
            );
    }
}



export default connect(null, {updateModalVisibility, showPicModal,updatePicModal})(Album);



const styles = StyleSheet.create({
    imagePallet: {
        flex: 1,
        margin: 1,
        top: 0,
        height: Dimensions.get('window').width/3-1,

        maxWidth : Dimensions.get('window').width/3-2,
        backgroundColor: GLOBAL.COLOR.TERTIARY
    },
    list: {
        justifyContent: 'flex-end',
        flexDirection: 'column',

    }
});