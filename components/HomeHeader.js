import React from 'react';

import {
    Text,
    StyleSheet
} from 'react-native';

import {
    LinearGradient,
} from "expo";

const HomeHeader = () => {
    return (
        <LinearGradient
            style={{
                flex: 1,
                paddingLeft: 17,
                justifyContent: 'center',
                alignSelf: 'stretch'
            }}
            start={[0, 0.5]}
            colors={['#4D6EFF', '#8087FF']}>
            <Text style={styles.homeHeaderUserText}>
                Hello, Arun!
            </Text>
        </LinearGradient>
    );
};

export default HomeHeader;

const styles = StyleSheet.create({
    homeHeaderContainer: {
        flex: 2,
        paddingLeft: 17,
        justifyContent: 'center',
        alignSelf: 'stretch'
    },
    homeHeaderUserText: {
        fontFamily: 'sf-pro-display-bold',
        fontSize: 35,
        textAlign: 'left',
        color: "white"
    },
});