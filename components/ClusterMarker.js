import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

import GLOBAL from '../Globals';

const ClusterMarker = ({count}) => {
    return (
        <View style={styles.container}>
            <View style={styles.bubble}>
                <Text style={styles.count}>{count}</Text>
            </View>
            <View style={styles.arrowBorder}/>
            <View style={styles.arrow}/>
        </View>
    );
};


export default ClusterMarker;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignSelf: 'flex-start',
    },
    bubble: {
        flex: 0,
        flexDirection: 'row',
        alignSelf: 'flex-start',
        backgroundColor: GLOBAL.COLOR.SECONDARY,
        paddingHorizontal: 10,
        paddingVertical: 6,
        borderRadius: 3,
        borderColor: GLOBAL.COLOR.TERTIARY,
        borderWidth: 0.5,
    },
    count: {
        color: GLOBAL.COLOR.TERTIARY,
        fontSize: 13,
    },
    arrow: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        borderTopColor: GLOBAL.COLOR.SECONDARY,
        alignSelf: 'center',
        marginTop: -9,
    },
    arrowBorder: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        borderTopColor: GLOBAL.COLOR.TERTIARY,
        alignSelf: 'center',
        marginTop: -0.5,
    },
});