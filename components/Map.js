import React, {Component} from 'react';
import Album from './Album';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableHighlight,
    Button,
    Modal,
} from 'react-native';
import {MapView} from 'expo';
import {fetchMapPoints,updateImageUrls,  updateModalVisibility} from '../actions/mapactions'

import ClusterMarker from './ClusterMarker';
import {getRegion, getCluster} from './MapUtils';
import {
    connect
} from 'react-redux';

import GLOBAL from '../Globals'

const {
    width,
    height
} = Dimensions.get('window');

const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 18.0;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class TrashMap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            region: null,
            errorMessage: "",
           
            imageUrlArray: []
        };

        this.renderMarker = this.renderMarker.bind(this);
    }

    toggleModal(visible) {
        this.setState({modalVisible: visible});
    }

    componentWillMount() {
        this.props.fetchMapPoints();
        const {coords} = this.props.location;
        this.setState({
            region: {
                latitude: coords.latitude,
                longitude: coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
        });
        this.props.fetchMapPoints();
    }

    increment(lat, long) {
        const region = getRegion(
            lat,
            long,
            this.state.region.latitudeDelta * 0.5
        );

        this.setState({region});
    }

    decrement(lat, long) {
        const region = getRegion(
            lat,
            long,
            this.state.region.latitudeDelta / 0.5
        );

        this.setState({region});
    }

    handleClusterMarkerPress(marker) {
        this.setState({imageUrlArray: marker.imageUrlArray});
        this.props.updateImageUrls(marker.imageUrlArray);
        this.props.updateModalVisibility(true);
                                
    }


    onRegionChange = (region) => {
        if (region.latitudeDelta < 0.5 * this.state.region.latitudeDelta) {
            this.increment(region.latitude,region.longitude);
        }
        else if (region.latitudeDelta * 0.5 > this.state.region.latitudeDelta) {
            this.decrement(region.latitude, region.longitude);
        }
    };

    renderMarker(marker, _index) {
        const key = _index + marker.geometry.coordinates[0];
        if (marker.properties) {
            return (
                <MapView.Marker
                    key={`${key}${marker.properties.clusterId}`}
                    coordinate={{latitude: marker.geometry.coordinates[1], longitude: marker.geometry.coordinates[0]}}
                    onPress={() => this.handleClusterMarkerPress(marker)}>
                    <ClusterMarker count={marker.properties.point_count}/>
                </MapView.Marker>
            )
        }
        else {
            return (
                <MapView.Marker
                    key={key}
                    coordinate={{latitude: marker.geometry.coordinates[1], longitude: marker.geometry.coordinates[0]}}/>
            )
        }
    }


    
    onButtonPress = () => {
        //precomputation over here
        this.props.fetchMapPoints();

    };


    render() {

        const places = this.props.markers.map(place => {
            return {
                geometry: {
                    coordinates: [
                        place.coordinates.longitude,
                        place.coordinates.latitude
                    ]
                },
                properties: {
                    imageUrl: place.imageUrl

                }

            }
        });


        const cluster = getCluster(places, this.state.region);
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    region={this.state.region}
                    onRegionChange={this.onRegionChange}>
                    {
                        cluster.markers.map(this.renderMarker)
                    }
                </MapView>
                <View style={styles.buttonContainer}>
                    <Button
                        large
                        title="Refresh"
                        backgroundColor="#009688"
                        icon={{name: 'refresh'}}
                        onPress={this.onButtonPress}
                    />
                </View>


            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        markers: state.mapReducer.markers,
        location: state.locationReducer.location,
        top_left: state.locationReducer.top_left,
        top_right: state.locationReducer.top_right,
        bottom_left: state.locationReducer.bottom_left,
        bottom_right: state.locationReducer.bottom_right,
        isModalVisible: state.mapReducer.isModalVisible
    };
}


export default connect(mapStateToProps, {fetchMapPoints, updateImageUrls, updateModalVisibility})(TrashMap);

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: GLOBAL.COLOR.PRIMARY
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },


    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
    text: {
        color: GLOBAL.COLOR.SECONDARY,
        marginTop: 10
    },
    modal: {
        justifyContent: 'flex-end',


    },
    modalInner:{
        justifyContent: 'flex-end',
        flex:1,
        backgroundColor:'white'
    },

});
