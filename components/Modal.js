import React, {Component} from 'react';
import Album from './Album';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    Touchable,
    TouchableOpacity,
    Button,
    Modal,
} from 'react-native';
import {updateModalVisibility} from '../actions/mapactions'

import {
    connect
} from 'react-redux';

import GLOBAL from '../Globals';

class TrashAlbumHeader extends Component{
    constructor(props){
        super(props);
    }

    render() {
        return (<View style={{flex:3,height:Dimensions.get('window').height*0.05}}>
            <TouchableOpacity onPress={() => {
                this.props.updateModalVisibility(false);
            }}
                              style={{flex:1,justifyContent: 'flex-end',alignItems: 'center'}}

            >
                <Text style = {{flex:3,}}>

                    Images Uploaded in Area
                </Text>

            </TouchableOpacity>

        </View>);

    }
}

const TrashAlbumHeaderContainer = connect(null, {updateModalVisibility})(TrashAlbumHeader);



class TrashModal extends Component {
    constructor(props) {
        super(props);
        console.log('Inside Modal');
    }  

    render() {
        return (<View style = {{flex:1,backgroundColor:'black'}}>
                <Modal animationType={"slide"} transparent={true}
                           visible={this.props.isModalVisible}

                           onRequestClose={() => {
                                this.props.updateModalVisibility(false)
                           }}>
                    <View style={{flex:1, top:50}}>
                        <View style={{flex:1,borderTopLeftRadius: 25,
                        borderTopRightRadius: 25, overflow:'hidden',justifyContent: 'flex-end',backgroundColor:'white'}}>
                                    <TrashAlbumHeaderContainer/>
                        </View>


                        <View style={{flex:11}}>
                                <Album data={this.props.imageUrlArray}/>
                        </View>
                    </View>
                </Modal>
                </View>
               );

        }
}






function mapStateToProps(state) {


    return {
        imageUrlArray: state.mapReducer.imageUrlArray,
        isModalVisible: state.mapReducer.isModalVisible
    };
}




export default connect(mapStateToProps, { updateModalVisibility})(TrashModal);
