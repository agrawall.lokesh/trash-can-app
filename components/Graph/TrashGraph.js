import React from "react";
import {View, Text} from "react-native"

import {AreaChart, YAxis, XAxis} from 'react-native-svg-charts'
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';

import {
    Svg
} from "expo";

const {Circle} = Svg;


const TrashGraph = ({xAxisData, yAxisData}) => {
    if (yAxisData === []) {
        return (
            <View style={{
                flex: 3,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Text>Loading graph...</Text>
            </View>
        )
    }

    return (
        <View
            style={{
                flex: 5,
                flexDirection: 'row'
            }}
        >
            <View style={{width: 50, flexDirection: 'column'}}>
                <YAxis
                    style={{flex: 1}}
                    data={yAxisData}
                    contentInset={{top: 30, bottom: 20}}
                    min={0}
                    svg={{
                        fill: '#707070',
                        fontSize: 10,
                    }}
                />
                <View
                    style={{
                        flex: 0.1
                    }}
                />
            </View>
            <View style={{flex: 1, flexDirection: 'column'}}>
                <AreaChart
                    style={{flex: 1}}
                    data={yAxisData}
                    contentInset={{top: 30, bottom: 10, right: 10, left: 8}}
                    svg={{
                        fill: '#FEE8E4',
                        stroke: "#FF9786",
                        strokeWidth: 3
                    }}
                    showGrid={false}
                    gridMin={0}
                    curve={shape.curveMonotoneX}
                    xScale={scale.scaleTime}
                    renderDecorator={({x, y, index, value}) => (
                        <Circle
                            key={index}
                            cx={x(index)}
                            cy={y(value)}
                            r={5}
                            stroke={'#FF9786'}
                            strokeWidth={3}
                            fill={'white'}
                        />
                    )}
                />
                <XAxis
                    style={{flex: 0.1}}
                    data={xAxisData}
                    formatLabel={(value) => value.toLocaleDateString('en-US', {
                        month: 'short',
                        day: 'numeric'
                    })}
                    contentInset={{left: 16}}
                    xAccessor={({item}) => new Date(item)}
                    svg={{
                        fill: '#707070',
                        fontSize: 10,
                    }}
                    min={0}
                    numberOfTicks={5}
                    scale={scale.scaleTime}
                />
            </View>
        </View>
    );
};

export default TrashGraph;