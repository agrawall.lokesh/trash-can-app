import React from "react";
import {
    View,
    StyleSheet
} from "react-native"

import GraphTabText from "./GraphTabText";

const GraphTabs = ({activeTab, tabTextList, setActiveTab}) => {
    return (
        <View style={styles.graphTabContainer}>
            {
                tabTextList.map(
                    (text, idx) => (
                        <GraphTabText
                            isActive={parseInt(activeTab) === idx}
                            tabText={text}
                            onPress={() => setActiveTab(idx)}
                            key={idx}  // Probably use unique key. May cause bugs
                        />
                    )
                )
            }
        </View>
    );
};

export default GraphTabs;

GraphTabs.defaultProps = {
    activeTab: 0,
    tabTextList: ['all  ', 'last 7 ', 'last 30']
};

const styles = StyleSheet.create({
    graphTabContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        borderColor: "#DCDADA",
        borderBottomWidth: 1
    }
});