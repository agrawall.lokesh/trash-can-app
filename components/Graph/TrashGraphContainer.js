import React, {Component} from "react";

import {View} from 'react-native';

import {connect} from "react-redux";

import {updateGraphImageCount, updateImageCount, setActiveTab} from "../../actions/homepageactions";
import TrashGraph from './TrashGraph';
import GraphSummary from './GraphSummary';
import GraphTabs from "./GraphTabs";

import {isValidDate} from "../../utils/util"


class TrashGraphContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dateAxisData: [],
            countAxisData: [],
            activeTab: -1,
            trashCount: 0
        }
    }

    setGraphState(filteredTrashGraphCountData) {
        let dateList = filteredTrashGraphCountData.map(obj => new Date(obj.date));
        let imageCountList = filteredTrashGraphCountData.map(obj => obj.count);

        let totalImageCount = imageCountList.reduce(function (a, b) {
            return a + b;
        }, 0);

        this.setState({
            dateAxisData: dateList,
            countAxisData: imageCountList,
            activeTab: this.props.activeTabIndex,
            trashCount: totalImageCount
        });
    }

    setFilteredGraphImageCount(filteredTrashGraphCountData, days) {
        if (days !== null) {
            filteredTrashGraphCountData = filteredTrashGraphCountData.filter(obj =>
                isValidDate(obj, days)
            );
        }
        this.setGraphState(filteredTrashGraphCountData);

    }

    async componentWillMount() {
        await this.props.updateGraphImageCount();

        if (this.props.trashGraphImageCountData !== null) {
            this.setGraphState(this.props.trashGraphImageCountData)
        }
    }

    shouldComponentUpdate(nextProp) {
        return (
            this.props.trashGraphImageCountData !== null &&
            (nextProp.activeTabIndex !== this.state.activeTab)
        );
    }

    componentWillUpdate() {
        if ((this.props.trashGraphImageCountData !== null)) {
            if (this.props.activeTabIndex === 0) {
                this.setFilteredGraphImageCount(this.props.trashGraphImageCountData, null);
            }
            if (this.props.activeTabIndex === 1) {
                this.setFilteredGraphImageCount(this.props.trashGraphImageCountData, 7);
            }
            else if (this.props.activeTabIndex === 2) {
                this.setFilteredGraphImageCount(this.props.trashGraphImageCountData, 30);
            }
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <GraphTabs
                    activeTab={this.props.activeTabIndex}
                    setActiveTab={this.props.setActiveTab}
                />

                <GraphSummary
                    activeIndex={this.props.activeTabIndex}
                    trashCount={this.state.trashCount}
                />

                <TrashGraph
                    xAxisData={this.state.dateAxisData}
                    yAxisData={this.state.countAxisData}
                />
            </View>
        );
    }
}

function mapStateToProps({homePageReducer}) {
    return {
        trashGraphImageCountData: homePageReducer.trashGraphImageCountData,
        activeTabIndex: homePageReducer.activeTabIndex
    }
}

export default connect(mapStateToProps, {
    updateGraphImageCount,
    updateImageCount,
    setActiveTab
})(TrashGraphContainer);