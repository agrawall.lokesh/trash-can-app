import React from "react";

import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import AnimateNumber from 'react-native-animate-number';


const GraphSummary = ({activeIndex, trashCount}) => {
    let graphCountText = " in Total.";

    if (activeIndex === 1) {
        graphCountText = " in the last 7 days."
    }
    else if (activeIndex === 2) {
        graphCountText = " in the last 30 days."
    }

    return (
        <View style={styles.graphSummaryContainer}>
            <AnimateNumber
                style={styles.graphSummaryCount}
                value={trashCount}
                formatter={(val) => {
                    return '' + parseInt(val) + ' '
                }}
                interval={1}
                steps={1}
                timing={(interval, progress) => {
                    // slow start, slow end
                    return interval * (1 - Math.sin(Math.PI * progress)) * 10
                }}/>
            <Text style={styles.graphSummaryCountText}>
                Pictures Shared
                <Text>
                    {graphCountText}
                </Text>
            </Text>
        </View>
    );
};

export default GraphSummary;

const styles = StyleSheet.create({
    graphSummaryContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 10,
    },
    graphSummaryCount: {
        fontWeight: 'bold',
        color: "#010003",
        fontSize: 16,
    },
    graphSummaryCountText: {
        color: "#010003",
        fontSize: 16,
        fontFamily: "sf-pro-text-regular"
    }
});