import React from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native"

const GraphTabText = ({isActive, tabText, onPress}) => {
    return (
        <View style={styles.tabTextContainer}>
            <Text
                onPress={onPress}
                style={[styles.tabText, {
                    color: isActive ? "#5170FF" : "#DCDADA"
                }]}
            >
                {tabText.toUpperCase()}
            </Text>
        </View>
    );
};

export default GraphTabText;


GraphTabText.defaultProps = {
    isActive: false
};

const styles = StyleSheet.create({
    tabTextContainer: {
        flex: 1,
        alignItems: 'center'
    },
    tabText: {
        fontWeight: 'bold',
        fontFamily: 'sf-pro-display-bold',
    }
});