import React, {Component} from 'react';
import Album from './Album';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    Touchable,
    TouchableOpacity,
    Button,
    Modal,
} from 'react-native';
import {updatePicModal} from '../actions/mapactions'

import {
    connect
} from 'react-redux';

import {Image} from 'react-native-expo-image-cache';
import GLOBAL from '../Globals';




class PicModal extends Component {
    constructor(props) {
        super(props);
        console.log('Inside Modal');
    }  

    render() {
       
        return (<View style = {{flex:1,backgroundColor:'black'}}>
                <Modal animationType={"slide"} transparent={false}
                           visible={this.props.isPicModalVisible}
                           style={{backgroundColor:'black'}} 
                           onRequestClose={() => {
                           }}>
                  <View style={{flex:1,backgroundColor:'black'}}>         
                      <TouchableOpacity onPress={() => {
                                    this.props.updatePicModal(false);
                                }}
                                style={{flex:1,justifyContent: 'flex-end'}}

                                >

                                <Text style={{color:'white'}}>

                                    BACK
                                </Text>
                                
                      </TouchableOpacity>
                      <Image  style={{flex:5}} {...{uri:this.props.currentImageUrl}} />
                      <View style={{flex:1,backgroundColor:'black'}}/>
                      </View>
                 </Modal>
                </View>
               );

        }
}


function mapStateToProps(state) {


    return {
        currentImageUrl: state.mapReducer.currentImageUrl,
        isPicModalVisible: state.mapReducer.isPicModalVisible
    };
}


export default connect(mapStateToProps, {updatePicModal})(PicModal);
