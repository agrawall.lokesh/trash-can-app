import {
    Location
} from "expo";

// ------ Location Utils -------
export default getLocationAsync = async () => {
    return await Location.getCurrentPositionAsync({});
};

// ------ General Utils -------

export function isValidDate(obj, days) {
    let d1 = new Date();
    let d2 = new Date(obj.date);

    let diff = Math.abs(d1.getTime() - d2.getTime());
    let dayDiff = Math.round(diff / (1000 * 60 * 60 * 24));

    return days > dayDiff
}