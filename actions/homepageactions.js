import {
    SET_SHOW_CAMERA_COMPONENT,
    SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
    NAVIGATE_TO_HOME_SCREEN,
    CLEAR_SHARE_IMAGE_STATE,
    USER_LOCATION,
    UPDATE_IMAGE_COUNT,
    UPDATE_GRAPH_IMAGE_COUNT,
    SET_IMAGE_TAGS,
    SET_TAG_TEXT,
    SET_ACTIVE_GRAPH_TAB,
    S3_UPLOAD_DATA,
    S3_UPLOAD_ERROR,
    MONGO_UPLOAD_DATA,
    MONGO_UPLOAD_ERROR,
    REFRESH_GRAPH_DATA,
    REFRESH_GRAPH_DATA_ERROR
} from './types';
import axios from "axios/index";

import GLOBAL from '../Globals'
import querystring from "querystring";


const TRASH_IMAGE_DAILY_COUNT_URL = GLOBAL.GO_SERVER.BASE_URL + '/getDailyTrashImageCount';


export const setShowCameraComponent = () => {
    return {
        type: SET_SHOW_CAMERA_COMPONENT
    }
};

export const setShowCapturedImageAndUnsetShowCameraComponent = (imageUri, imageBase64) => {
    return {
        type: SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
        image: {uri: imageUri, base64: imageBase64}
    }
};

export const navigateToHomeScreen = () => {
    return {
        type: NAVIGATE_TO_HOME_SCREEN
    }
};

export const clearShareImageScreenState = () => {
    return {
        type: CLEAR_SHARE_IMAGE_STATE
    }
};

export const setUserLocation = (location) => {
    return {
        type: USER_LOCATION,
        location: location
    }
};

export const updateImageCount = (count) => {
    // TODO: Some double rendering happending. Need to fix.
    return {
        type: UPDATE_IMAGE_COUNT,
        trashImageCount: count
    }
};

export const updateGraphImageCount = () => {
    return async (dispatch) => {
        let data = await axios.get(
            TRASH_IMAGE_DAILY_COUNT_URL
        );

        dispatch({
            type: UPDATE_GRAPH_IMAGE_COUNT,
            trashImageData: data
        });
    }
};

export const setImageTags = (clarifaiTags, userTags, tagText) => {
    return {
        type: SET_IMAGE_TAGS,
        clarifaiTags: clarifaiTags,
        userTags: userTags,
        tagText: tagText
    }
};

export const setTagText = (text) => {
    return {
        type: SET_TAG_TEXT,
        tagText: text,
    }
};

export const setActiveTab = (activeTabIndex) => {
    return {
        type: SET_ACTIVE_GRAPH_TAB,
        activeTabIndex: activeTabIndex
    }
};

const UPLOAD_HEADER = {
    'Content-Type': 'application/x-www-form-urlencoded'
};

export const uploadImageDataToS3 = async (base64) => {
    const S3_UPLOAD_IMAGE_URL = GLOBAL.NODE_SERVER.BASE_URL + "/upload";

    try {
        const response = await axios.post(
            S3_UPLOAD_IMAGE_URL,
            {photo: base64},
            UPLOAD_HEADER
        );

        return {
            type: S3_UPLOAD_DATA,
            imageUrl: response.data,
        };
    }
    catch (error) {
        return {
            type: S3_UPLOAD_ERROR,
            error: error
        };
    }
};

export const uploadImageUrlToMongo = async (imageUrl, location) => {
    const UPLOAD_IMAGE_URL = GLOBAL.GO_SERVER.BASE_URL + "/uploadTrashImage";

    try {
        const mongoQueryString = querystring.stringify({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            imageUrl: imageUrl
        });

        const response = await axios.post(
            UPLOAD_IMAGE_URL,
            mongoQueryString,
            UPLOAD_HEADER
        );

        return {
            type: MONGO_UPLOAD_DATA,
            response: response
        };
    }
    catch (error) {
        return {
            type: MONGO_UPLOAD_ERROR,
            error: error
        };
    }
};

export const refreshGraphData = async () => {
    const response = await axios.get(
        TRASH_IMAGE_DAILY_COUNT_URL
    );

    try {
        return {
            type: REFRESH_GRAPH_DATA,
            trashImageData: response
        };
    }
    catch (error) {
        return {
            type: REFRESH_GRAPH_DATA_ERROR,
            error: error
        };
    }
};

export const uploadImageToServer = (base64Image, location) => {
    return async (dispatch) => {
        const s3Response = await uploadImageDataToS3(base64Image);
        if (s3Response.type === S3_UPLOAD_ERROR) {
            dispatch({type: S3_UPLOAD_ERROR, error: s3Response.error});
        }
        else {
            const mongoResponse = await uploadImageUrlToMongo(s3Response.imageUrl, location);

            if (mongoResponse.type === MONGO_UPLOAD_ERROR) {
                dispatch({type: MONGO_UPLOAD_ERROR, error: mongoResponse.error});
            }
            else {
                const graphDataResponse = await refreshGraphData();
                if (graphDataResponse.type === REFRESH_GRAPH_DATA_ERROR) {
                    dispatch({type: REFRESH_GRAPH_DATA_ERROR, error: mongoResponse.error});
                }
                else {
                    // navigate back to home screen
                    dispatch({type: NAVIGATE_TO_HOME_SCREEN, error: null})
                }
            }
        }
    }
};