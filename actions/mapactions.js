import axios from 'axios';

const MAP_URL = 'http://trashcandev.herokuapp.com/getTrashImages';
import  {MAP_POINTS, UPDATE_MARKERS , UPDATE_IMAGE_URLS, MODAL_VISIBLE, PIC_MODAL_VISIBLE, PIC_MODAL} from './types';
export const fetchMapPoints = () => async (dispatch) => {
    let {data} = await axios.get(MAP_URL);

    const payload = data.map((x) => {
        return {coordinates: {latitude: x.location.coordinates[0], longitude: x.location.coordinates[1]},imageUrl:x.imageUrl}
    });

    dispatch({
        type: MAP_POINTS,
        payload: {"markers": payload}
    });
};

export const updateMarkers = (markers) => {
    return {
        type: UPDATE_MARKERS,
        payload: {"markers": markers}
    }
};

export const updateImageUrls = (imageUrls=[]) => {
    if (imageUrls === 'undefined'){
        imageUrls = [];
    }

    return {
        type: UPDATE_IMAGE_URLS,
        payload: {"imageUrlArray": imageUrls}
    }
};

export const updateModalVisibility = (isModalVisible =false) => {
    if (isModalVisible === 'undefined'){
        isModalVisible = false;
    }
    return {
        type:MODAL_VISIBLE,
        payload:{
            isModalVisible:isModalVisible    
        }
    }
};

export const updatePicModal = (isPicModalVisible ) => {
    console.log('##$');
    console.log(isPicModalVisible);
   return {
        type:PIC_MODAL_VISIBLE,
        payload:{
           isPicModalVisible:isPicModalVisible 
        }
   }     
};
export const showPicModal = (currentImageUrl = null) => {
   return {
        type:PIC_MODAL,
        payload:{
            currentImageUrl:currentImageUrl
        }
   }     
};