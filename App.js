import React from 'react';
import {Provider} from 'react-redux';
import {Asset, AppLoading, Font} from 'expo';
import firebase from 'firebase';

import Drawer from './components/Drawer';
import store from './store';


function cacheImages(images) {
    return images.map(image => {
        if (typeof image === 'string') {
            return Image.prefetch(image);
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
}


function cacheFonts(fonts) {
    return fonts.map(font => Font.loadAsync(font));
}

export default class App extends React.Component {
    state = {
        isReady: false,
    };

    componentDidMount() {
        // Initialize Firebase
        const config = {
            apiKey: "AIzaSyDIpSjIVPztP_4g-iGbjuAeZO297sQ6I6A",
            authDomain: "trashcan-1520169011747.firebaseapp.com",
            databaseURL: "https://trashcan-1520169011747.firebaseio.com",
            projectId: "trashcan-1520169011747",
            storageBucket: "trashcan-1520169011747.appspot.com",
            messagingSenderId: "811137564544"
        };
        firebase.initializeApp(config);
    }

    render() {
        process.nextTick = setImmediate;
        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this._cacheResourcesAsync}
                    onFinish={() => this.setState({isReady: true})}
                    onError={console.warn}
                />
            );
        }

        return (
            <Provider store={store}>
                <Drawer/>
            </Provider>
        );
    }

    async _cacheResourcesAsync() {
        const images = [];

        const imageAssets = cacheImages(images);

        const fonts = [
            {'sf-pro-display-bold': require('./assets/fonts/SF-Pro-Display-Bold.otf')},
            {'sf-pro-text-bold': require('./assets/fonts/SF-Pro-Text-Bold.otf')},
            {'sf-pro-text-semibold': require('./assets/fonts/SF-Pro-Text-Semibold.otf')},
            {'sf-pro-text-regular': require('./assets/fonts/SF-Pro-Text-Regular.otf')}

        ];

        const fontAssets = cacheFonts(fonts);

        await Promise.all([...imageAssets, ...fontAssets]);
    }
}
