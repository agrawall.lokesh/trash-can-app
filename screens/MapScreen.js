import React, {Component} from 'react';

import TrashMap from '../components/Map';
import TrashModal from '../components/Modal';

import PicModal from '../components/PicPreview';

import {
	 StyleSheet,
    View
} from 'react-native';
export default class Home extends Component {
    render() {
        return (

        			<View style={styles.container}>    	
        	    		<TrashMap/>
        	    		<TrashModal/>
                        <PicModal/>
        	    	</View>
                
        	);
    }
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    }

});
