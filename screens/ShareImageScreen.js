import React, {Component} from "react";
import {StyleSheet, View, StatusBar} from 'react-native';
import {connect} from "react-redux";
import {HeaderBackButton} from "react-navigation";
import {axios} from 'axios';
import {setShowCameraComponent, clearShareImageScreenState} from "../actions/homepageactions";
import {PreviewImage} from "../components";

import GLOBAL from '../Globals'

class ShareImageScreen extends Component {
    static navigationOptions = {
        title: 'Share Post',
        headerTintColor: "#010003",
        headerTitleStyle: {
            fontSize: 17,
            fontFamily: 'sf-pro-text-semibold',
        },
    };

    componentDidMount() {
        this.props.navigation.setParams({onBackButtonPress: this.onBackButtonPress});
    }

    checkInitialState = (props) => {
        return (props.showCameraComponent === false &&
            props.capturedImage === null &&
            props.navigateToHomeScreen === false);
    };

    componentWillReceiveProps(nextProps) {
        if (this.checkInitialState(nextProps)) {
            this.onBackButtonPress(false);
        }
    }

    onBackButtonPress = (clearState = true) => {
        if (clearState) {
            this.props.clearShareImageScreenState();
        }
        this.props.navigation.goBack();
    };

    render() {
        if (this.props.capturedImage !== null) {
            return (
                <PreviewImage
                    capturedImage={this.props.capturedImage}
                    onBackButtonPress={() => this.onBackButtonPress()}
                />
            )
        }
        else {
            return <View style={styles.container}/>
        }
    }
}

function mapStateToProps(state) {
    return {
        showCameraComponent: state.homePageReducer.showCameraComponent,
        capturedImage: state.homePageReducer.capturedImage,
        navigateToHomeScreen: state.homePageReducer.navigateToHomeScreen
    }
}

export default connect(mapStateToProps, {
    setShowCameraComponent, clearShareImageScreenState
})(ShareImageScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GLOBAL.COLOR.PRIMARY
    }
});