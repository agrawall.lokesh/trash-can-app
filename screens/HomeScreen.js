import React, {Component} from 'react';
import {connect} from "react-redux";
import {CameraWithPermission} from "../components";
import DropdownAlert from 'react-native-dropdownalert';
import TrashGraphContainer from '../components/Graph/TrashGraphContainer';
import HomeHeader from '../components/HomeHeader';

import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {
    Button,
} from 'react-native-elements';

import {
    Permissions,
} from "expo";

import getLocationAsync from '../utils/util';

import {
    setUserLocation, setShowCameraComponent
} from "../actions/homepageactions";


class HomeScreen extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: null
        };
    }

    navigateToShareImageScreen = () => {
        this.props.navigation.navigate('share_image_screen');
    };

    onCameraButtonPress = () => {
        this.props.setShowCameraComponent();
    };

    async componentWillMount() {
        let {status} = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Please enable location permission to share trash images.',
            });
        }
        else {
            let location = await getLocationAsync();
            this.props.setUserLocation(location);
        }
    }

    shouldComponentUpdate(nextProp) {
        return (
            this.props.showCameraComponent !== nextProp.showCameraComponent
        );
    }

    componentDidUpdate() {
        if (this.state.errorMessage !== null) {
            this.dropdown.alertWithType('error', 'Error', this.state.errorMessage);
        }
    }

    render() {
        if (this.props.showCameraComponent) {
            return <CameraWithPermission onImageCapture={this.navigateToShareImageScreen}/>
        }

        return (
            <View style={styles.container}>
                <View style={styles.homeScreenUpperSection}>
                    <HomeHeader/>
                </View>
                <View style={styles.homeScreenLowerSection}>
                    <View style={styles.homeScreenTrashGraphContainer}>
                        <TrashGraphContainer/>
                    </View>
                    <View style={styles.homeScreenCameraViewContainer}>
                        <View style={styles.homeScreenCameraViewTextContainer}>
                            <Text style={styles.homeScreenCameraViewText}>Add a Photo to Share</Text>
                        </View>
                        <View style={styles.homeScreenCameraView}>
                            {
                                this.state.errorMessage === null ? (
                                    <Button
                                        large
                                        buttonStyle={styles.homeScreenCameraButton}
                                        icon={{name: 'photo-camera'}}
                                        onPress={() => this.onCameraButtonPress()}
                                    />
                                ) : (<View/>)
                            }
                        </View>
                    </View>
                </View>
                <DropdownAlert
                    ref={ref => this.dropdown = ref}
                    closeInterval={null}
                />
            </View>
        );
    }
}

function mapStateToProps({homePageReducer}) {
    return {
        showCameraComponent: homePageReducer.showCameraComponent,
    }
}

export default connect(mapStateToProps, {
    setUserLocation, setShowCameraComponent
})(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    homeScreenUpperSection: {
        flex: 1,
    },

    homeScreenLowerSection: {
        flex: 3,
        backgroundColor: "#F4F4F4",
        zIndex: 2,
        paddingLeft: 23,
        paddingRight: 23
    },

    homeScreenTrashGraphContainer: {
        flex: 2,
        backgroundColor: "white"
    },

    homeScreenCameraViewContainer: {
        flex: 1,
        marginTop: 30,
        marginBottom: 30,
        backgroundColor: "white"
    },

    homeScreenCameraViewTextContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },

    homeScreenCameraViewText: {
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: "sf-pro-text-semibold"
    },

    homeScreenCameraView: {
        flex: 2,
        alignItems: 'stretch',
        justifyContent: 'center'
    },

    homeScreenCameraButton: {
        backgroundColor: "#7882FF",
        height: 50,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5
    }

});