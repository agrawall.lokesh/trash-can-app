export default {
    COLOR: {
        PRIMARY: "#1abc9c",
        SECONDARY: "#c0392b",
        TERTIARY: "#bdc3c7"
    },
    CLARIFAI: {
        API_KEY: "b3228bd2e87447e7971b1c5a30358a70",
        IMAGE_ACCURACY_THRESHOLD: 0.1,
        MODEL: "trashcan-v2"
    },
    GEO_CODER: {
        API_KEY: "AIzaSyCl7YViIrmuNb18A4W1rX4hDC2bD_lW91w",
    },
    GO_SERVER: {
        BASE_URL: "https://trashcandev.herokuapp.com",
    },
    NODE_SERVER: {
        BASE_URL: "https://trashcannodeserver.herokuapp.com",
    }
};